const express = require("express");
const seed = require("./routes/seed/products");
const app = express();

/**
 * Pull in some configs etc, package.json has some values
 */
app.set("package", require("../package.json"));

// set view engine
app.set("view engine", "ejs");
app.set("views", "./src/views");

// set base static path (serves your ./public files as-is)
app.use("/static", express.static(__dirname + "/public"));

// jquery
app.use(
  "/static/vendor/jquery",
  express.static(__dirname + "/../node_modules/jquery/dist")
);
//
// bootstrap
// - set vendor static path (defines a route to the modules dist as-is)
app.use(
  "/static/vendor/bootstrap",
  express.static(__dirname + "/../node_modules/bootstrap/dist")
);

//import the mongoose module
const mongoose = require("mongoose");

//set up default mongoose connection
const mongoDB = "mongodb://mongo:27017/shop_db";
mongoose.connect(mongoDB); // Get Mongoose to use the global promise library
mongoose.Promise = global.Promise; //Get the default connection
const db = mongoose.connection;

//bind connection to error event (to get notification of connection errors)
db.on("error", console.error.bind(console, "MongoDB connection error:"));

//use seed to gen data
app.use("/seed", seed);

// main routes
app.use("/", require("./routes/solution1"));

// problem1 page
app.use("/problem1", require("./routes/solution1"));
app.use("/problem2", require("./routes/solution2"));
app.use("/problem3", require("./routes/solution3"));

// start server
app.listen(process.env.port || 8000);
console.log("Running at Port 8000");
