function month(s) {
  var months = [
    "มกราคม",
    "กุมภาพันธ์",
    "มีนาคม",
    "เมษายน",
    "พฤษภาคม",
    "มิถุนายน",
    "กรกฎาคม",
    "สิงหาคม",
    "กันยายน",
    "ตุลาคม",
    "พฤศจิกายน",
    "ธันวาคม",
  ];
  return months[parseInt(s) - 1];
}

function year(s) {
  var y = parseInt(s) + 543;
  return y.toString();
}

function day_of_week(d, m, y) {
  var D = parseFloat(d, 64);
  var M = parseFloat(m, 64);
  var Y = parseFloat(y, 64);
  var mon = 0.0;
  if (M > 2) {
    mon = 12 + M;
    Y--;
  } else {
    mon = M;
  }
  var Y = Y % 100;
  var C = Y / 100.0;
  var W =
    D +
    Math.floor((13 * (mon + 1)) / 5) +
    Y +
    Math.floor(Y / 4) +
    Math.floor(C / 4) +
    5 * C;
  W = W % 7;
  var DayOfWeeks = [
    "อาทิตย์",
    "จันทร์",
    "อังคาร",
    "พุธ",
    "พฤหัสบดี",
    "ศุกร์",
    "เสาร์",
  ];
  return DayOfWeeks[parseInt(W)];
}

function date_formatter(date_str) {
  var s = date_str.toString().split("/");
  var result =
    "วัน" +
    day_of_week(s[0], s[1], s[2]) +
    "ที่ " +
    s[0] +
    " " +
    month(s[1]) +
    " " +
    year(s[2]);

  return result;
}

const router = require("express").Router();

// SOLUTION#2
router.get("/", function (req, res) {
  var result = "";
  if (req.query.date_str === undefined) {
    result == "error";
  } else {
    result = date_formatter(req.query.date_str);
  }
  res.render("problem2", { result: result });
});

module.exports = router;
