const request = require("request");
const router = require("express").Router();

// SOLUTION#3
router.get("/", function (req, res) {
  var name = req.query.name;
  var options = {
    url: "https://jsonplaceholder.typicode.com/users?name=" + name,
    method: "GET",
    headers: {
      Accept: "application/json",
      "Accept-Charset": "utf-8",
      "User-Agent": "my-reddit-client",
    },
  };
  request(options, function (err, output, body) {
    var result = JSON.parse(body);
    res.render("problem3", { result: result });
  });
});

module.exports = router;
