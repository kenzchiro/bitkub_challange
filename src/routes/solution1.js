function result_html(result) {
  //console.log(result);
  var html = "";
  for (var i = 0; i < result.length; i++) {
    if (result[i] == "X") {
      html += "<div class='X'>" + result[i] + "</div>";
    } else if (result[i] == "\n") {
      html += "</br>";
    } else {
      html += "<div class='Other'>" + result[i] + "</div>";
    }
  }
  return html;
}

function pattern_control(pattern, N) {
  var result = "";
  if (pattern == 1) {
    result = pattern_one(N);
  } else if (pattern == 2) {
    result = pattern_two(N);
  } else if (pattern == 3) {
    result = pattern_three(N);
  } else {
    result = "";
  }
  return result;
}

function pattern_one(N) {
  var result = "";
  for (var i = 0; i < N; i++) {
    tmp = 0;
    for (var j = 0; j < N * 2 - 1; j++) {
      if (j < i) {
        result += "O";
      } else if (i < N * 2 - 1 - j) {
        if (tmp == 0) {
          result += "X";
          tmp++;
        } else {
          result += "O";
          tmp--;
        }
      } else {
        result += "O";
      }
    }
    result += "\n";
  }
  for (var i = N - 1; i > 0; i--) {
    var tmp = 0;
    for (var j = 0; j < N * 2 - 1; j++) {
      if (i - 1 > j) {
        result += "O";
      } else if (i < N * 2 - j) {
        if (tmp == 0) {
          result += "X";
          tmp++;
        } else {
          result += "O";
          tmp--;
        }
      } else {
        result += "O";
      }
    }
    result += "\n";
  }
  return result;
}

function pattern_two(N) {
  var result = "";
  var K = 0;
  if (N % 2 == 1) {
    K = N / 2 + 1;
  } else {
    K = N / 2;
  }
  N = N / 2;
  // devide pattern to four part
  // upper half of the pattern
  for (var i = 0; i < K; i++) {
    // upper left
    for (var j = 0; j < K; j++) {
      if (i >= j) {
        result += "X";
      } else {
        result += ".";
      }
    }
    // upper right
    for (var j = 0; j < N; j++) {
      if (i >= N - 1 - j) {
        result += "X";
      } else {
        result += ".";
      }
    }
    result += "\n";
  }

  // bottom half of the pattern
  for (var i = 0; i < N; i++) {
    // bottom left
    for (var j = 0; j < K; j++) {
      if (i + j < N) {
        result += "X";
      } else {
        result += ".";
      }
    }
    // bottom right
    for (var j = 0; j < N; j++) {
      if (i <= j) {
        result += "X";
      } else {
        result += ".";
      }
    }
    result += "\n";
  }
  return result;
}

function pattern_three(N) {
  var result = "";
  for (var i = 0; i < N; i++) {
    for (var j = 0; j < N; j++) {
      // reflect (i, j) to the top left quadrant as (a, b)
      var a = j;
      var b = i;
      if (a >= N / 2) {
        a = N - a - 1;
      }
      if (b >= N / 2) {
        b = N - b - 1;
      }

      // calculate distance from center ring
      var u = a - N / 2;
      if (u < 0) {
        u = u * -1;
      }
      var v = b - N / 2;
      if (v < 0) {
        v = v * -1;
      }
      var d = 0;
      if (u > v) {
        d = u;
      } else {
        d = v;
      }
      var L = N / 2;
      if (N % 3 == 0 && N > 3) {
        L--;
      }
      // fix the top-left-to-bottom-right diagonal
      if (i == j + 1 && i <= L) {
        d++;
      }
      if ((d + N / 2) % 2 == 0 || N <= 2) {
        result += "X";
      } else {
        result += "Y";
      }
    }
    result += "\n";
  }
  return result;
}

const router = require("express").Router();

// SOLUTION#1
router.get("/", function (req, res) {
  // grab previously set config/model whatnot from app
  var input = req.query.N;
  var pattern = req.query.pattern;
  var result = result_html(pattern_control(pattern, input));

  // render the ejs file, passing some values to it
  res.render("problem1", { result: result });
});

module.exports = router;
