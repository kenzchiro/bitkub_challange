## Prerequisite

- Docker - docker-compose,
- Node.js - npm

## Getting started

Start developing by run // or detach if you would like
STEP:1

```
$ git clone git@gitlab.com:kenzchiro/bitkub_challange.git
```

STEP:2

```
$ cd bitkub_challange
$ npm install
```

STEP:3

```
$ make dev
or
$ docker-compose up --build
```

\*\*\*NOTE

```
let's see source code in following path
- problem1 /bitkub_challange/src/routes/solution1.js
- problem2 /bitkub_challange/src/routes/solution2.js
- problem3 /bitkub_challange/src/routes/solution3.js
- problem4 /bitkub_challange/src/routes/models.js
```
